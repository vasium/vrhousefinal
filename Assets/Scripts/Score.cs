﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private TextMeshProUGUI TextPro;
    private static int score;

    void Start()
    {
        TextPro = gameObject.GetComponent<TextMeshProUGUI>();
        score = 0;
    }

    void Update()
    {
        TextPro.text = "Score: " + score;
    }

    public static void AddScore()
    {
        score += 100;
    }
}