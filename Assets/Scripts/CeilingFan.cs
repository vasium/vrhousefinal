﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CeilingFan : MonoBehaviour
{
    public float fanSpeed = 100f;

    void Update()
    {
        transform.Rotate(0, 0, fanSpeed * Time.deltaTime);
    }
}
