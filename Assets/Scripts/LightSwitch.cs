﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : MonoBehaviour
{
    public GameObject lightObject;
    private Light myLight;

    void Start()
    {
        myLight = lightObject.GetComponent<Light>();
    }

    public void LightOnOff()
    {
        myLight.enabled = !myLight.enabled;
    }
}